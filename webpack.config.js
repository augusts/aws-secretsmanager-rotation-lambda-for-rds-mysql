// serverless/plugin-webpack-config.js: Webpack configuration for serverless lambda functions.
const fs = require("fs");
const path = require("path");
const YAML = require("yaml");
const nodeExternals = require("webpack-node-externals");

// Following the defaults documented in serverless-components/aws-lambda
const { inputs: { src = "./src", handler = "handler.handler" } = {} } =
  YAML.parse(fs.readFileSync("serverless.yml", { encoding: "utf-8" })) ?? {};

const { src: srcPath, dist: distPath } = typeof src === "string" ? { src, dist: src } : src;

const [basename] = String(handler).split(".", 2);

const createWebpackConfig = async () => ({
  entry: { [basename]: `${srcPath}/${basename}.ts` },
  output: {
    filename: "[name].js",
    path: path.normalize(`${__dirname}/${distPath}`),
  },
  devtool: "source-map",
  externals: [
    nodeExternals({
      modulesFromFile: {
        excludeFromBundle: ["devDependencies"],
      },
    }),
  ],
  mode: "production",
  module: {
    rules: [
      {
        test: /\.(t|j)s$/,
        exclude: /(node_modules|node_modules\.nosync|bower_components)/,
        use: "ts-loader",
      },
    ],
  },
  performance: { maxAssetSize: 10485760, maxEntrypointSize: 10485760 },
  resolve: {
    extensions: [".js", ".ts"],
    fallback: {
      assert: false,
      constants: false,
      crypto: false,
      dns: false,
      fs: false,
      http: false,
      https: false,
      net: false,
      os: false,
      path: false,
      stream: false,
      timers: false, // mysql
      tls: false,
      util: false,
      vm: false,
      zlib: false,
    },
  },
  stats: {
    builtAt: false,
    children: false,
    chunks: false,
    colors: true,
    entrypoints: false,
    hash: false,
    modules: false,
    version: false,
  },
});

module.exports = createWebpackConfig();
