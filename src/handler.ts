import assert from "assert";
import type { AWSError, SecretsManager } from "aws-sdk";
import AWS from "aws-sdk";
import { Connection, createConnection } from "mysql";

type Event = {
  ClientRequestToken: string;
  SecretId: string;
  Step: String;
};

type MySQLCredential = {
  engine: string;
  port?: number;
  host: string;
  username: string;
  password: string;
  dbname: string;

  /**
   * In a multi-users rotation setup, this is the secrets for a master account
   * with permissions to create a new user and change passwords.
   */
  masterarn?: string;

  /**
   * Optional identifier when target database is a cluster.
   */
  dbClusterIdentifier?: string;
};

// AWS.config.logger = console;
AWS.config.httpOptions = {
  connectTimeout: 2000,
  timeout: 5000,
};

module.exports.handler = async ({ ClientRequestToken, SecretId, Step }: Event) => {
  console.info(`${Step}:`, { SecretId, ClientRequestToken });

  const client = new AWS.SecretsManager();

  const { RotationEnabled = false, VersionIdsToStages: { [ClientRequestToken]: version = [] } = {} } = await client
    .describeSecret({ SecretId })
    .promise();

  if (!RotationEnabled) {
    throw new Error(`Secret ${SecretId} is not enabled for rotation.`);
  }

  if (version.length === 0) {
    throw new Error(`Secret version ${ClientRequestToken} has no stage for the rotation of ${SecretId}.`);
  }

  if (version.includes("AWSCURRENT")) {
    throw new Error(`Secret version ${ClientRequestToken} already set as AWSCURRENT for secret ${SecretId}.`);
  }

  if (!version.includes("AWSPENDING")) {
    throw new Error(`Secret version ${ClientRequestToken} not set as AWSPENDING for rotation of secret ${SecretId}.`);
  }

  if (Step === "createSecret") {
    await createSecret(client, SecretId, ClientRequestToken);
  } else if (Step === "setSecret") {
    await updateSecret(client, SecretId, ClientRequestToken);
  } else if (Step === "testSecret") {
    await ensureSecret(client, SecretId, ClientRequestToken);
  } else if (Step === "finishSecret") {
    await finishSecret(client, SecretId, ClientRequestToken);
  } else {
    throw new Error(`Invalid secret rotation step ${Step} for secret ${SecretId}.`);
  }
};

function isAWSError(value: any): value is AWSError {
  return typeof value === "object" && typeof value.code === "string" && typeof value.message === "string";
}

const createSecret = async (client: SecretsManager, SecretId: string, token: string) => {
  const secret = await querySecret(client, SecretId, "AWSCURRENT");

  // If no secret exists, create one.
  try {
    await querySecret(client, SecretId, "AWSPENDING", token);

    console.info(`Pending secret already exists, skipping.`);
  } catch (e) {
    if (isAWSError(e) && e.code === "ResourceNotFoundException") {
      if (secret.masterarn) {
        secret.username = getAlternateUsername(secret.username);
      }

      const { RandomPassword } = await client
        .getRandomPassword({
          ExcludeCharacters: process.env.EXCLUDE_CHARACTERS ?? "/@\"'\\",
        })
        .promise();

      if (RandomPassword === undefined) {
        throw new Error(`Unable to generate a random password.`);
      }

      secret.password = RandomPassword;

      await client
        .putSecretValue({
          SecretId,
          ClientRequestToken: token,
          SecretString: JSON.stringify(secret),
          VersionStages: ["AWSPENDING"],
        })
        .promise();

      console.info(`Successfully created AWSPENDING for ${SecretId}@${token}.`);
    } else {
      throw e;
    }
  }
};

const updateSecret = async (client: SecretsManager, SecretId: string, token: string) => {
  const currentSecret = await querySecret(client, SecretId, "AWSCURRENT");
  const pendingSecret = await querySecret(client, SecretId, "AWSPENDING", token);

  // If pending secret already works, return.
  if (await ensureConnection(pendingSecret)) {
    console.info(`Pending secret for ${SecretId} already checks out.`);
    return;
  }

  // Make sure username and host to be updated are correct.
  if (
    pendingSecret.username !==
    (pendingSecret.masterarn ? getAlternateUsername(currentSecret.username) : currentSecret.username)
  ) {
    throw new Error(`Username mismatch between current and pending secrets.`);
  } else if (pendingSecret.host !== currentSecret.host) {
    throw new Error(`Host mismatch between current and pending secrets.`);
  }

  // multi-users rotation
  if (pendingSecret.masterarn) {
    if (null === (await ensureConnection(currentSecret))) {
      throw new Error(`Unable to connect with current secret: [${SecretId}].`);
    }

    const masterSecret = await querySecret(client, pendingSecret.masterarn, "AWSCURRENT");
    if (
      currentSecret.host !== masterSecret.host ||
      (!currentSecret.dbClusterIdentifier && !isReplicaHost(currentSecret, masterSecret))
    ) {
      throw new Error(`Current host is neither matches the master one, nor a replica of it.`);
    }

    const connection = await ensureConnection(masterSecret);
    assert(connection, `Unable to connect with master secret: [${pendingSecret.masterarn}].`);

    try {
      const [[count = 0] = []] = await queryConnection(
        connection,
        "SELECT COUNT(*) as count FROM mysql.user WHERE User = ?",
        [pendingSecret.username],
      );

      if (count === 0) {
        await queryConnection(connection, "CREATE USER ? IDENTIFIED BY ?", [
          pendingSecret.username,
          pendingSecret.password,
        ]);
      }

      // Grants all current priviledges to the pending user.
      {
        const rows = await queryConnection(connection, "SHOW GRANTS FOR ?", [currentSecret.username]);

        for (const row of rows) {
          const grant = row[0].split(" TO ")[0].replace(/%/g, "%%");
          await queryConnection(connection, `${grant} TO ?`, [pendingSecret.username]);
        }
      }

      await queryConnection(connection, `SET PASSWORD FOR ? = ${await getPasswordOption(connection)}`, [
        pendingSecret.username,
        pendingSecret.password,
      ]);
    } finally {
      await closeConnection(connection);
    }
  }
  // single-user rotation
  else {
    let connection = await ensureConnection(currentSecret);

    if (connection === undefined) {
      const previousSecret = await querySecret(client, SecretId, "AWSPREVIOUS").catch(() => null);
      if (previousSecret !== null) {
        if (previousSecret.username !== pendingSecret.username) {
          throw new Error(
            `Not modifying user ${pendingSecret.username}, it's not the same as previous valid user ${previousSecret.username}.`,
          );
        } else if (previousSecret.host !== pendingSecret.host) {
          throw new Error(
            `Not mofidying host ${pendingSecret.host}, it's not the same as previous valid host ${previousSecret.host}.`,
          );
        }

        connection = await ensureConnection(previousSecret);
      }
    }

    assert(connection, `Unable to connect with previous, current and pending secret in ${SecretId}.`);

    try {
      await queryConnection(connection, `SET PASSWORD = ${await getPasswordOption(connection)}`, [
        pendingSecret.password,
      ]);
    } finally {
      await closeConnection(connection);
    }
  }
};

const ensureSecret = async (client: SecretsManager, SecretId: string, token: string) => {
  const pendingSecret = await querySecret(client, SecretId, "AWSPENDING", token);
  const connection = await ensureConnection(pendingSecret);
  assert(connection, `Unable to connect with pending secret in ${SecretId}.`);

  const [[version, now]] = await queryConnection(connection, `SELECT VERSION(), NOW()`);

  console.info(
    `Successfully connected to ${pendingSecret.engine}@${version} with pending secret, current server time is ${now}.`,
  );
};

const finishSecret = async (client: SecretsManager, SecretId: string, token: string) => {
  const { VersionIdsToStages = {} } = await client.describeSecret({ SecretId }).promise();

  const [version] = Object.entries(VersionIdsToStages).find(([, stages]) => stages.includes("AWSCURRENT")) ?? [];

  if (version === undefined) {
    console.error(`AWSCURRENT is not found in secret ${SecretId}.`);
    return;
  }

  if (version === token) {
    console.info(`Version ${version} is already mark as AWSCURRENT for ${SecretId}.`);
  } else {
    await client
      .updateSecretVersionStage({
        SecretId,
        VersionStage: "AWSCURRENT",
        MoveToVersionId: token,
        RemoveFromVersionId: version,
      })
      .promise();

    console.info(`Successfully set AWSCURRENT stage for ${SecretId}.`);
  }
};

const querySecret = async (
  client: SecretsManager,
  SecretId: string,
  VersionStage: string,
  VersionId: string | undefined = undefined,
) => {
  // console.debug(`GetSecretValue:`, { SecretId, VersionStage, VersionId });

  const { SecretString } = await client.getSecretValue({ SecretId, VersionStage, VersionId }).promise();

  if (SecretString === undefined) {
    throw new Error(`Empty secret retrieved for ${SecretId}.`);
  }

  const secret = JSON.parse(SecretString);

  if (secret.engine !== "mysql") {
    throw new Error(`Database engine must be 'mysql' to use this lambda.`);
  }

  if (!isMySQLCredential(secret)) {
    throw new Error(`Stored secret is not a valid MySQL credential.`);
  }

  return secret;
};

const getAlternateUsername = (username: string): string => {
  const altSuffix = "_alt";

  if (username.endsWith(altSuffix)) {
    return username.slice(0, -altSuffix.length);
  }

  return `${username}${altSuffix}`;
};

function isMySQLCredential(value: any): value is MySQLCredential {
  return (
    typeof value === "object" &&
    ["host", "username", "password"].every((key) => typeof value[key] === "string" && value[key])
  );
}

const ensureConnection = async ({
  port,
  dbname,
  host,
  username,
  password,
}: MySQLCredential): Promise<Connection | undefined> => {
  try {
    const connection = createConnection({
      database: dbname,
      host,
      password,
      port,
      user: username,
    });

    await queryConnection(connection, "SELECT NOW()");

    return connection;
  } catch (e) {
    console.warn(`Error connecting to MySQL:`, e);
  }
};

const queryConnection = (connection: Connection, query: string, parameters?: any[]) =>
  new Promise<any>((resolve, reject) => {
    connection.query(query, parameters, (error, result, fields) => {
      // console.debug(`Query`, { query, parameters, error, result, fields });

      if (error) {
        reject(error);
      } else if (Array.isArray(result)) {
        resolve(result.map((row: any) => Object.values(row)));
      } else {
        resolve(result);
      }
    });
  });

const closeConnection = (connection: Connection) =>
  new Promise<void>((resolve, reject) => {
    connection.end((err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });

const isReplicaHost = async (
  { host: replicaHost, dbClusterIdentifier: replicaId = replicaHost.split(".")[0] }: MySQLCredential,
  { host: masterHost, dbClusterIdentifier: masterId = masterHost.split(".")[0] }: MySQLCredential,
): Promise<boolean> => {
  try {
    const { DBInstances } = await new AWS.RDS().describeDBInstances({ DBInstanceIdentifier: replicaId }).promise();

    return DBInstances?.[0].ReadReplicaDBInstanceIdentifiers?.includes(masterId) ?? false;
  } catch (e) {
    console.error(`Unable to retrieve RDS instances.`, e);
  }

  return false;
};

const getPasswordOption = async (connection: Connection) => {
  const [[version]] = await queryConnection(connection, "SELECT VERSION()");
  return parseInt(version[0]) >= 8 ? `?` : `PASSWORD(?)`;
};

// TODO: [ ] Support resourcePolicy via lambda.addPermission to allow secretsmanager to invoke this lambda.
